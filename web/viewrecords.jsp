<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<jsp:useBean id="result" type="java.sql.ResultSet" scope="request"/>
<!DOCTYPE html>
<html>
<head>
  <title>View Results</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style>
	body, h1,h2,h3,h4,h5,h6 {font-family: "Montserrat", sans-serif}
	.w3-row-padding img {margin-bottom: 12px}

	.w3-sidebar {width: 120px;background: #222;}

	#main {margin-left: 120px}
	
	@media only screen and (max-width: 600px) {#main {margin-left: 0}}
  </style>
 
</head>

<body class="w3-black">
	
  <div class="w3-top w3-hide-large w3-hide-medium" id="myNavbar">
	  <div class="w3-bar w3-black w3-opacity w3-hover-opacity-off w3-center w3-small">
	     <%@ include file = "design/header.html" %>
	  </div>
  </div>
	
  <nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center">
 
  <img src="images/mrtlogo.png" style="width:100%">
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-black">
    <i class="fa fa-home w3-xxlarge"></i>
    HOME
  </a>
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-user w3-xxlarge"></i>
    ABOUT
  </a>
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-envelope w3-xxlarge"></i>
    CONTACT
  </a>
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-envelope w3-xxlarge"></i>
    LOCATIONS
  </a>
 </nav>
 
 	<div class="w3-padding-large" id="main">
		<header class="w3-container w3-padding-32 w3-center w3-black" id="home">
		    <h1 class="w3-jumbo"><span class="w3-hide-small">Our</span> MRT Station.</h1>
		    <p>We'll take you where you need to go.</p>
		    <img src='images/mrtlogo.png' class="w3-image" width='992' height='1108' alt='logo'/>
	    </header> 
	        
	  <table class="table table-responsive table-sm table-inverse">
	    <thead>
	      <tr>
	        <th>First Name</th>
	        <th>Last Name</th>
	        <th>Stop Name</th>
	        <th>Stop Number</th>
	        <th>Fare</th>
	      </tr>
	    </thead>
	    <tbody>
	    	<%
				while (result.next()) {
			%>
	      <tr align="center">
	      	<td><%= result.getString("firstName") %></td>
			<td><%= result.getString("lastName") %></td>
			<td><%= result.getString("stopName") %></td>
			<td><%= result.getInt("stopNo") %></td>
			<td><%= result.getInt("fare") %></td>
		  </tr>
	       <% } %>
	    </tbody>
	  </table>
		
		<form action="index.jsp">
			<button class="w3-button w3-light-grey w3-padding-large" type="submit">
				<i class="fa fa-paper-plane"></i> BACK
			</button>
		</form>
		
		<footer class="container-fluid text-center">
			<%@ include file="design/footer.jsp"%>
		</footer>
	</div>
</body>
</html>