<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   
<%@ page import="model.*, controller.*" %>

<!DOCTYPE html>
<html>
<head>
  <title>MRT Station Display</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style>
	body, h1,h2,h3,h4,h5,h6 {font-family: "Montserrat", sans-serif}
	.w3-row-padding img {margin-bottom: 12px}

	.w3-sidebar {width: 120px;background: #222;}

	#main {margin-left: 120px}
	
	@media only screen and (max-width: 600px) {#main {margin-left: 0}}
  </style>
</head>
<body class="w3-black">
	<div class="w3-top w3-hide-large w3-hide-medium" id="myNavbar">
	  <div class="w3-bar w3-black w3-opacity w3-hover-opacity-off w3-center w3-small">
	     <%@ include file = "design/header.html" %>
	  </div>
  </div>
	
  <nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center">
 
  <img src="images/mrtlogo.png" style="width:100%">
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-black">
    <i class="fa fa-home w3-xxlarge"></i>
    HOME
  </a>
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-user w3-xxlarge"></i>
    ABOUT
  </a>
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-envelope w3-xxlarge"></i>
    CONTACT
  </a>
  <a href="index.jsp" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
    <i class="fa fa-envelope w3-xxlarge"></i>
    LOCATIONS
  </a>
 </nav>
 
  <div class="w3-padding-large" id="main">
		<header class="w3-container w3-padding-32 w3-center w3-black" id="home">
		    <h1 class="w3-jumbo"><span class="w3-hide-small">Our</span> MRT Station.</h1>
		    <p>We'll take you where you need to go.</p>
		    <img src='images/mrtlogo.png' class="w3-image" width='992' height='1108' alt='logo'/>
	    </header>
	    
	   <div class="w3-content w3-justify w3-text-grey w3-padding-64" id="about">
		   	<h2 class="w3-text-light-grey">RESULT</h2>
		   	<hr style="width:200px">
		   	<h3>Welcome ${mrt.firstName} ${mrt.lastName}! </h3><br/><br/>
			<p> ${mrt.message} </p>
		
			<p><a href="index.jsp">Click here to go back to form.</a></p>
			
			<form action = "getallrecords.html">
				<button class="w3-button w3-light-grey w3-padding-large" type="submit">
					<i class="fa fa-paper-plane"></i> View All Records
				</button>
			</form>
		
			<div class="w3-padding-64 w3-content w3-text-grey" id="contact">
				<footer class="container-fluid text-center">
			 		<p> <%@ include file="design/footer.jsp" %> </p>
			    </footer>
			</div>
		</div>
 </div>
	
	
</body>
</html>