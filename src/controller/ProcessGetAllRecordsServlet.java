package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;
import model.*;

public class ProcessGetAllRecordsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection connection = null;
		
		if (getServletContext().getAttribute("dbconn") != null) {
			connection = (Connection) getServletContext().getAttribute("dbconn");
		} else {
//			String jdbcUrl = getServletContext().getInitParameter("jdbcURL");
//			String dbUsername = getServletContext().getInitParameter("dbUsername");
//			String dbPassword = getServletContext().getInitParameter("dbPassword");
			
			connection = new MRTStationBean().getConnection();
		}
		
		ResultSet result = new MRTStationBean().getAllRecords(connection);
		
		//perform binding
		request.setAttribute("result", result);
		
		request.getRequestDispatcher("viewrecords.jsp").forward(request, response);
	}

}
