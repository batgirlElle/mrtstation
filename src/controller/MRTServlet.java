package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;

import error.*;
import model.*;

public class MRTServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MRTStationBean mrt = new MRTStationBean();
	Connection connection=null;
	String jdbcURL=null;
	String dbUsername=null;
	String dbPassword=null;

	public void init(){
		int[] stopPrices = { Integer.parseInt(getServletContext().getInitParameter("stopQuezon")),
				Integer.parseInt(getServletContext().getInitParameter("stopGMA")),
				Integer.parseInt(getServletContext().getInitParameter("stopCubao")),
				Integer.parseInt(getServletContext().getInitParameter("stopSantolan")),
				Integer.parseInt(getServletContext().getInitParameter("stopOrtigas")),
				Integer.parseInt(getServletContext().getInitParameter("stopShaw")),
				Integer.parseInt(getServletContext().getInitParameter("stopBoni")),
				Integer.parseInt(getServletContext().getInitParameter("stopGuadalupe")),
				Integer.parseInt(getServletContext().getInitParameter("stopBuendia")),
				Integer.parseInt(getServletContext().getInitParameter("stopAyala")),
				Integer.parseInt(getServletContext().getInitParameter("stopMagallanes")),
				Integer.parseInt(getServletContext().getInitParameter("stopTaft"))};

		mrt.setStopPrices(stopPrices);
//		jdbcURL=getServletContext().getInitParameter("jdbcURL");
//		dbUsername=getServletContext().getInitParameter("dbUsername");
//		dbPassword=getServletContext().getInitParameter("dbPassword");

		connection = mrt.getConnection();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String exceptionMessage = "";
		int exceptionFlagTrigger = 0; //0 means no error, 1 means there is an error

		//HttpSession session = request.getSession();
		ResultSet result = mrt.getAllRecords(connection);

		int stop1=0, stop2=0, stop3=0, stop4=0, stop5=0, stop6=0;

		try {
			while(result.next()){
				if(result.getInt("stopNo")==1){
					stop1+=result.getInt("fare");
				}
				if(result.getInt("stopNo")==2){
					stop2+=result.getInt("fare");
				}
				if(result.getInt("stopNo")==3){
					stop3+=result.getInt("fare");
				}
				if(result.getInt("stopNo")==4){
					stop4+=result.getInt("fare");
				}
				if(result.getInt("stopNo")==5){
					stop5+=result.getInt("fare");
				}
				if(result.getInt("stopNo")==6){
					stop6+=result.getInt("fare");
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

  if(getServletContext().getAttribute("stop1")==null){
  			getServletContext().setAttribute("stop1", stop1);
  		}
  		if(getServletContext().getAttribute("stop2")==null){
  			getServletContext().setAttribute("stop2", stop2);
  		}
  		if(getServletContext().getAttribute("stop3")==null){
  			getServletContext().setAttribute("stop3", stop3);
  		}
  		if(getServletContext().getAttribute("stop4")==null){
  			getServletContext().setAttribute("stop4", stop4);
  		}
  		if(getServletContext().getAttribute("stop5")==null){
  			getServletContext().setAttribute("stop5", stop5);
  		}
  		if(getServletContext().getAttribute("stop6")==null){
  			getServletContext().setAttribute("stop6", stop6);
  		}

		try {
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			int stopNo = Integer.parseInt(request.getParameter("stopNo"));

			if(stopNo%10>2 || stopNo%10<1 || stopNo/10>6 || stopNo/10<1){
				exceptionMessage+="Invalid Stop!<br/> ";
				exceptionFlagTrigger=10;
			}

			if (firstName.isEmpty()) {
				exceptionMessage += "Missing first name. ";
				exceptionFlagTrigger += 1;
			}

			if (lastName.isEmpty()) {
				exceptionMessage += "Missing last name. ";
				exceptionFlagTrigger += 1;
			}

			if (exceptionFlagTrigger ==0) { //all are valid input data
				mrt.setFirstName(firstName);
//				System.out.println(mrt.getFirstName());
				mrt.setLastName(lastName);
//				System.out.println(mrt.getLastName());
				mrt.setStopNo(stopNo);
//				System.out.println(mrt.getStopNo());
				mrt.compute((Integer)getServletContext().getAttribute("stop1"), (Integer)getServletContext().getAttribute("stop2"),
						(Integer)getServletContext().getAttribute("stop3"), (Integer)getServletContext().getAttribute("stop4"),
						(Integer)getServletContext().getAttribute("stop5"), (Integer)getServletContext().getAttribute("stop6"));
				getServletContext().setAttribute("stop1", mrt.getStopProfits()[0]);
				getServletContext().setAttribute("stop2", mrt.getStopProfits()[1]);
				getServletContext().setAttribute("stop3", mrt.getStopProfits()[2]);
				getServletContext().setAttribute("stop4", mrt.getStopProfits()[3]);
				getServletContext().setAttribute("stop5", mrt.getStopProfits()[4]);
				getServletContext().setAttribute("stop6", mrt.getStopProfits()[5]);

				mrt.insertRecord(connection);
				getServletContext().setAttribute("mrt", mrt);
			} else {
				if(exceptionFlagTrigger<10){
					throw new RuntimeException(exceptionMessage);
				}
				else{
					throw new InvalidDestinationException(exceptionMessage);
				}
			}
		} catch (RuntimeException re) {
				getServletContext().setAttribute("errmsg", exceptionMessage);
		}
				request.getRequestDispatcher((exceptionFlagTrigger == 0)
				? "display.jsp":"error.jsp").forward(request, response);
	}
}
