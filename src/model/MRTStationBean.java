package model;

import java.sql.*;

import javax.sql.*;
import javax.naming.*;

import util.SQLCommand;

public class MRTStationBean implements SQLCommand {
//	private static int
//	stopQuezon = 10,
//	stopGMA = 10,
//	stopCubao = 11,
//	stopSantolan = 11,
//	stopOrtigas = 12,
//	stopShaw = 12,
//	stopBoni = 13,
//	stopGuadalupe = 13,
//	stopBuendia = 14,
//	stopAyala = 14,
//	stopMagallanes = 15,
//	stopTaft = 15;
	
	//input values
	private int stopNo;
	private String firstName;
	private String lastName;
	private String message;
	private int[] stopPrices=new int[12];
	private int[] stopProfits= new int[6];
	//computed values
	private int fare;
	
	public int[] getStopPrices() {
		return stopPrices;
	}
	public void setStopPrices(int[] stopPrices) {
		this.stopPrices = stopPrices;
	}
	
	public int getFare() {
		return fare;
	}
	public void setFare(int fare) {
		this.fare = fare;
	}

	public int getStopNo() {
		return stopNo;
	}
	public void setStopNo(int stopNo) {
		this.stopNo = stopNo;
	}
	public int[] getStopProfits() {
		return stopProfits;
	}
	public void setStopProfits(int stopProfits[]) {
		this.stopProfits = stopProfits;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String stopName(int stopNo){
		String returnName;
		switch(stopNo){
		case 11:
			returnName = "Quezon Avenue";
			break;
		case 12:
			returnName= "GMA Kamuning";
			break;
		case 21:
			returnName= "Cubao";
			break;
		case 22:
			returnName= "Santolan";
			break;
		case 31:
			returnName= "Ortigas";
			break;
		case 32:
			returnName= "Shaw Boulevard";
			break;
		case 41:
			returnName= "Boni Avenue";
			break;
		case 42:
			returnName= "Guadalupe";
			break;
		case 51:
			returnName= "Buendia";
			break;
		case 52:
			returnName= "Ayala";
			break;
		case 61:
			returnName= "Magallanes";
			break;
		case 62:
			returnName= "Taft Avenue";
			break;
		default:
			returnName= "ERROR";
		}
		return returnName;
	}
	
	public void compute(int stop1, int stop2, int stop3, int stop4, int stop5, int stop6){
		String message="";

		switch(this.stopNo){
		case 11:
			fare = stopPrices[0];
			stop1 += fare;
			break;
		case 12:
			fare = stopPrices[1];
			stop1 += fare;
			break;
		case 21:
			fare = stopPrices[2];
			stop2 += fare;
			break;
		case 22:
			fare = stopPrices[3];
			stop2 += fare;
			break;
		case 31:
			fare = stopPrices[4];
			stop3 += fare;
			break;
		case 32:
			fare = stopPrices[5];
			stop3 += fare;
			break;
		case 41:
			fare = stopPrices[6];
			stop4 += fare;
			break;
		case 42:
			fare = stopPrices[7];
			stop4 += fare;
			break;
		case 51:
			fare = stopPrices[8];
			stop5 += fare;
			break;
		case 52:
			fare = stopPrices[9];
			stop5 += fare;
			break;
		case 61:
			fare = stopPrices[10];
			stop6 += fare;
			break;
		case 62:
			fare = stopPrices[11];
			stop6 += fare;
			break;
		default:
			fare = 0;
		}
		stopProfits[0]= stop1;
		stopProfits[1]= stop2;
		stopProfits[2]= stop3;
		stopProfits[3]= stop4;
		stopProfits[4]= stop5;
		stopProfits[5]= stop6;
		
		message=message+"The fare amount to " + stopName(this.stopNo).toUpperCase()
			+ " station from North Avenue Station is <b>P" + fare + ".00.<br/><br/>";
		message=message+"Please remember you have to alight at STOP " + (this.stopNo/10) + ".<br/>";
		
		message = message + "<hr style='width:200px'/>" + "<br/>";
		
		message = message+"LIST OF FARE PROFITS <br/><br/>";
	
		message = message+"TOTAL FARE FOR ALL STOP 1: P" + stop1 + ".00<br/>";
		message = message+"TOTAL FARE FOR ALL STOP 2: P" + stop2 + ".00<br/>";
		message = message+"TOTAL FARE FOR ALL STOP 3: P" + stop3 + ".00<br/>";
		message = message+"TOTAL FARE FOR ALL STOP 4: P" + stop4 + ".00<br/>";
		message = message+"TOTAL FARE FOR ALL STOP 5: P" + stop5 + ".00<br/>";
		message = message+"TOTAL FARE FOR ALL STOP 6: P" + stop6 + ".00<br/><br/><br/>";
		
		this.message = message;
		}
	
//	public Connection getConnection(String jdbcURL, String dbUsername, String dbPassword){
//		Connection connection=null;
//
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			connection = DriverManager.getConnection(jdbcURL,dbUsername,dbPassword);
//			}
//		catch (ClassNotFoundException cnfe) {
//			System.err.println("Class not found: "+cnfe.getMessage());
//		} catch (SQLException sqle) {
//			System.err.println("SQL Exception: " + sqle.getMessage());
//		}
//		return connection;
//	}
	public Connection getConnection() {
		Connection connection = null;
		
		try {			
			connection = ((DataSource) InitialContext
				.doLookup("java:/comp/env/jdbc/SE31Amazing"))
				.getConnection();
			
		} catch (NamingException ne) {
			System.err.println("Naming Exception: " 
				+ ne.getMessage());
		} catch (SQLException sqle) {
			System.err.println("SQL Exception: " 
				+ sqle.getMessage());
		}
		return connection;
	}
	
	public void insertRecord(Connection connection){
		if(connection!=null){
			try{
				//set auto-commit to false
				connection.setAutoCommit(false);
				
				PreparedStatement psmnt=connection.prepareStatement(INSERT_REC);
				psmnt.setString(1, this.firstName);
				psmnt.setString(2, this.lastName);
				psmnt.setString(3, stopName(this.stopNo));
				psmnt.setInt(4, this.stopNo/10);
				psmnt.setInt(5, this.fare);
				
				psmnt.executeUpdate();
				connection.commit();
			} catch(SQLException sqle){
				if(connection != null){
					 try {
						connection.rollback();
					} catch (SQLException e) {
						 throw new RuntimeException();
					}
				 }
			  throw new RuntimeException();
			}
		}
	}
	
	public ResultSet getAllRecords(Connection connection) {
		ResultSet result = null;
		
		 if (connection != null) {
			 try {
				 PreparedStatement pstmnt = connection.prepareStatement(GET_ALL_REC);
				 
				 //now use either query or update
				 result = pstmnt.executeQuery();
				 
			 } catch (SQLException sqle) {
				 throw new RuntimeException();
			 }
		 }
		 return result;
	}
}